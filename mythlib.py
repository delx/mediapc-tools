#! python3

import os
import socket

import lxml.etree
import MySQLdb

def get_config():
    with open(os.path.expanduser("~/.mythtv/config.xml")) as f:
        return lxml.etree.parse(f, lxml.etree.XMLParser(encoding="utf-8"))

def get_db_connection(config_xml):
    return MySQLdb.connect(
        host = config_xml.xpath("/Configuration/Database/Host/text()")[0],
        port = int(config_xml.xpath("/Configuration/Database/Port/text()")[0]),
        user = config_xml.xpath("/Configuration/Database/UserName/text()")[0],
        passwd = config_xml.xpath("/Configuration/Database/Password/text()")[0],
        db = config_xml.xpath("/Configuration/Database/DatabaseName/text()")[0],
    )

def get_recordings_dir(config_xml, db_connection):
    try:
        localhostname = config_xml.xpath("/Configuration/LocalHostName/text()")[0]
    except IndexError:
        localhostname = socket.gethostname()

    with db_connection.cursor(MySQLdb.cursors.DictCursor) as cursor:
        cursor.execute("""
            SELECT * FROM settings
            WHERE value='RecordFilePrefix' AND hostname='%s'
        """ % localhostname)

        return cursor.fetchone()["data"]
