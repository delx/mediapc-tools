# mediapc-tools

Scripts and cron helpers for a MediaPC / HTPC, particularly with MythTV.

## MythTV helpers

- `check-guide-data` -- Ensure TV guide is up to date for all channels.
- `mythcleandb` -- Delete up stale recordings in the MythTV database.
- `mythcleanfiles` -- Delete up empty failed recordings.
- `mythcleanpng` -- Delete orphaned PNG screenshots.
- `mythsymlink` -- Create symlinks to all recordings with friendly names.
- `mythtv-mysql-maintenance` -- Repair and optimise performance of the MythTV database tables.
- `show-mythtv-delays` -- Print stats on how long MythTV took to start playing videos.


## Desktop helpers

- `disable-screen-blank.desktop` -- Turn off DPMS so the screen always stays on.
- `eject.desktop` -- Put this on your desktop to easily eject `/dev/sr0`.
- `run-every-5sec.service` -- This is a big dumb convenient hammer.
- `xfdesktop-focus-fix` -- Eg run this every 5sec to fix the case where XFCE unfocuses the desktop.


## Notification helpers
- `gen-new-list` -- Create symlinks for new files into a `-NEW-` directory.
- `monitor-dir` -- Send email notification for files that have been added since the last run.
